document.addEventListener('DOMContentLoaded', function () {


    // Pour l'affichage des images
    let disp_img = document.querySelector(".product-images img")
    let disp_miniature = document.querySelectorAll(".product-miniatures img")

    disp_miniature.forEach(element => {
        element.addEventListener('click', () => {
            disp_img.src = element.src
        })
    })



    // Pour les boutons
    let minus_button = document.querySelector("#minus")
    let disp_number = document.querySelector("#quantity")
    let plus_button = document.querySelector("#plus")
    let maxQuantity = document.querySelector(".box")

    minus_button.addEventListener('click', () => {

        if( parseInt(disp_number.innerText) >1 && parseInt(disp_number.innerText) <=5 ) {
            let num = parseInt(disp_number.innerText) -1
            disp_number.innerText = num.toString()
            maxQuantity.style.visibility ="hidden"
        }

    })

    plus_button.addEventListener('click', () => {

        if(parseInt(disp_number.innerText) >= 1 && parseInt(disp_number.innerText)<5){
            let num = parseInt(disp_number.innerText) +1
            disp_number.innerText = num.toString()
        }
        else if( parseInt(disp_number.innertext) === 5 ){
            maxQuantity.style.visibility ="visible"
        }

    })


})