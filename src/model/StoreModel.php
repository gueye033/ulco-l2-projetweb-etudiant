<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }
  static function listProducts():array
  {
      // Connexion à la base de données
      $ab =\model\Model::connect();

      //Requete SQL
      $sql ="SELECT product.id, product.name,price,product.image, category.name as c_name FROM product INNER JOIN category ON product.category = category.id";
      //Exécute de la requete
      $req =$ab->prepare($sql);
      $req->execute();
      //Retourner les résultats (type array)
      return $req->fetchAll();
  }
  static function infoProduct(int $id):array
  {
      // Connexion à la base de données
      $abc =\model\Model::connect();
      //Requete SQL
      $sql="SELECT product.id, product.name,price,product.image, category.name as c_name FROM product INNER JOIN category WHERE product.category = category.id";
      //Exécute de la requete
      $req =$abc->prepare($sql);
      $req->execute();
      //Retourner les résultats (type array)
      return $req->fetchAll();
  }

}