<?php


namespace controller;


class AccountController
{
    public function account(): void
    {
        // Variables à transmettre à la vue
        $params = [
            "title" => "account",
            "module" => "account.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }
    public function login(): void
    {
        $trd=\model\AccountModel::login();
        if(isset($_POST['Connexion'])){
            if(empty($_POST['usermail']) || empty(($_POST['userpass']))){
                header('Location: /account');
                exit();
            }
            else{
                $nom=$_POST['userlastname'];
                $prnom=$_POST['userfirstname'];
                $email=$_POST['usermail'];
                $mdp=$_POST['userpass'];
                header('Location: /store');
                exit();
            }
        }
        else{
            echo "vous n'avez pas encore valider le formulaire";
        }
        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($trd);
    }
    public function signin(): void
    {
        $trd=\model\AccountModel::signin();
        if(isset($_POST['Inscription'])){
            if(empty($_POST['userlastname']) || empty($_POST['userfirstname']) || empty($_POST['usermail']) || empty($_POST['userpass'])){
                echo "veuillez remplir les champs";
            }
            else{
                $nom=$_POST['userlastname'];
                $prnom=$_POST['userfirstname'];
                $email=$_POST['usermail'];
                $mdp=$_POST['userpass'];
                header('Location: /account');
                exit();
            }
        }
        else{
            echo "vous n'avez pas encore valider le formulaire";
        }
        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($trd);
    }
    public function logout():void
    {
        session_start();
        if (session_destroy()) {
            header('Location: /account');
            exit();
        }

    }
}