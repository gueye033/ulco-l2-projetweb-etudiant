<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $produits = \model\StoreModel::listProducts();
    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
        "produits" => $produits
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }
    public function product(int $id): void
    {
        $info=\model\StoreModel::infoProduct($id);

        $params=array(
            "title"=>"Product",
            "module"=>"product.php",
            "info"=>$info
        );

        if ($params["info"]==null){
            header('Location: /store');
            exit();
        }
        else {
            \view\Template::render($params);
        }

    }

}