<?php
?>
<div  id="error">
    <h1>Oups...</h1>
    <p><i>On dirait que la page demandée n'existe pas !</i></p>
    <a href="/">Retour à l'accueil</a>
</div>
